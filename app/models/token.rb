class Token < ActiveRecord::Base
  belongs_to :user

  before_create :set_auth_token
  before_update :set_auth_token

  def self.generate_token(user)
    tokens = user.tokens.order(updated_at: :desc)
    if tokens.count == 2
      tokens.last.save
      tokens.last.touch
    else
      Token.create(:user_id => user.id)
    end
  end

  protected

    def set_auth_token
      self.access_value = loop do
        randomToken = SecureRandom.urlsafe_base64(32, false)
        break randomToken unless self.class.exists?(access_value: randomToken)
      end
    end

end
