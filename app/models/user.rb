class User < ActiveRecord::Base
  has_many :tokens

  before_save :hash_to_md5, if: :password_changed?

  validates :nickname, length: { in: 2..50 }, presence: true, uniqueness: true
  validates :email, length: { in: 4..255 }, presence: true, uniqueness: true
  validates :password, length: { minimum: 6 }

  private

    def hash_to_md5
      self.password = Digest::MD5.hexdigest(self.password)
    end

end
