json.user do
  json.id @user.id
  json.token @user.tokens.last.access_value
end
