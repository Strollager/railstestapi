json.token do
  json.id @userToken.id
  json.user_id @userToken.user_id
  json.value @userToken.access_value
end
