class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  private

    def auth_with_basic
      authenticate_or_request_with_http_basic do |nickname, password|
        password = Digest::MD5.hexdigest(password)
        @user = User.find_by(:nickname => nickname, :password => password)
        if @user.present?
          true
        else
          render json: { :message => "Access Denied" }, status: :unauthorized
        end
      end
    end

    def auth_with_token
      authenticate_or_request_with_http_token do |token|
        @token = Token.find_by(:access_value => token)
        if @token.present?
          @user = User.find_by(:id => @token.user_id)
          if @user.present?
            true
          else
            render json: { :message => "Access Denied" }, status: :unauthorized
          end
        else
          render json: { :message => "Wrong Token" }, status: :unauthorized
        end
      end
    end


end
