class UsersController < ApplicationController
  before_filter :auth_with_basic, only: [:auth]
  before_filter :auth_with_token, only: [:update]

  # для навигации по юзерам
  def index
  end

  # для регистрации юзера
  def create
    @user = User.create(user_params)
    if @user.errors.empty?
      @userToken = Token.create(user_id: @user.id)
			render :partial => 'users/create'
		else
			render json: @user.errors
		end
  end

  def show
  end

  # для обновления юзера
  def update
    @user = User.find_by_id(params[:id])
    @user.update_attributes(user_params)
    if @user.errors.empty?
      @userToken = Token.create(user_id: @user.id)
			render :partial => 'users/create'
		else
			render json: @user.errors
		end
  end

  # для авторизации юзера
  def auth
    Token.generate_token(@user)
  end

  private

		def user_params
			params.require(:user).permit(:nickname, :email, :password)
		end

end
