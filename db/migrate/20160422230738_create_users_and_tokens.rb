class CreateUsersAndTokens < ActiveRecord::Migration
  def change

    create_table :users do |t|
      t.string :nickname, :unique => true
      t.string :email,    :unique => true
      t.string :password
      t.timestamps
    end

    create_table :tokens do |t|
      t.belongs_to :user,     index: true
      t.string :access_value, :unique => true
    end

  end
end
