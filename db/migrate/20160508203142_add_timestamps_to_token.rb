class AddTimestampsToToken < ActiveRecord::Migration
  def change

    change_table :tokens do |t|
      t.timestamps
    end

  end
end
