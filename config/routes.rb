Rails.application.routes.draw do

  resources :users do
    collection do
      post 'auth'
    end
  end

end
